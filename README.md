# Blackchuck

TUI game similar to blackjack, written in Rust with Cursive.

<img src="assets/images/screenshot.png" alt="Screenshot">