use rand::{thread_rng, Rng};
use std::cmp::Ordering;
use std::fmt;

pub type Players = Vec<Player>;

#[derive(Clone)]
pub struct Game {
    pub turn: u8,
    pub players: Players,
    pub winner: Option<Player>
}

#[derive(Clone, PartialEq, Eq)]
pub struct Player {
    pub status: Status,
    pub name: String,
    pub roll_sum: u8,
    pub points: u32,
}

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub enum Status {
    TwentyOne,
    Playing,
    Bust,
}

impl Player {
    pub fn roll_dice(&mut self) {
        if self.status == Status::Playing {
            let mut rng = thread_rng();
            let dice_roll = rng.gen_range(1..=6);
            self.roll_sum += dice_roll;
            self.check_bust()
        }
    }
    fn check_bust(&mut self) {
        match self.roll_sum.cmp(&21) {
            Ordering::Greater => self.status = Status::Bust,
            Ordering::Equal => self.status = Status::TwentyOne,
            _ => self.status = Status::Playing,
        }
    }
}

impl Game {
    //Match the current player's status, and return it is he's playing
    pub fn whose_turn(&mut self) -> Option<&mut Player> {
        if self.turn < self.players.len() as u8 {
            match &mut self.players[self.turn as usize].status {
                Status::Playing => Some(&mut self.players[self.turn as usize]),
                _ => {
                    self.end_turn();
                    None
                }
            }
        } else {
            match self.get_winner() {
                Some(winner) => {
                    self.winner = Some(winner);
                },
                _ => {
                    self.semi_reset();
                },
            }
            None
        }
    }
    pub fn end_turn(&mut self) {
        self.turn += 1
    }

    pub fn get_winner(&mut self) -> Option<Player> {
        let mut previous_player: Option<&mut Player> = None;
        let mut previous_num = 0;
        let mut biggest_sum: Option<Player> = None;

        for player in self.players.iter_mut() {
            if player.status != Status::Bust {
                match player.roll_sum.cmp(&previous_num) {
                    Ordering::Greater => {
                        previous_num = player.roll_sum;
                        biggest_sum = Some(player.clone());
                        previous_player = Some(player);
                    },
                    Ordering::Equal => {
                        //Reset the roll sum
                        player.roll_sum = 0;

                        if let Some(prev_player) = previous_player { prev_player.roll_sum = 0 }
                        previous_player = Some(player);

                        biggest_sum = None;
                    },
                    _ => {
                        previous_player = Some(player);
                    }
                }
            }
        }
        biggest_sum
        // let winner = self.players.iter()
        //     .max_by_key(|player| player.roll_sum <= 21).unwrap().clone();

    }
    pub fn reset(&mut self) {
        self.turn = 0;
        for mut player in self.players.iter_mut() {
            player.roll_sum = 0;
            player.status = Status::Playing;
        }
    }
    //If there's no winner but the round is already over, only reset non-bust players
    pub fn semi_reset(&mut self) {
        self.turn = 0;
        for player in self.players.iter_mut() {
            match player.status {
                Status::Playing => player.roll_sum = 0,
                Status::TwentyOne => {
                    player.roll_sum = 0;
                    player.status = Status::Playing
                },
                _ => {}
            }
        }
    }
}

impl fmt::Display for Status {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Status::Bust => write!(f, "{:?}", self),
            Status::Playing => write!(f, ""),
            Status::TwentyOne => write!(f, "Twenty one!"),
        }
    }
}

pub fn create_game(player_count: u8) -> Game {
    //Create players
    let mut players: Players = Vec::with_capacity(player_count as usize);
    for i in 0..player_count {
        players.push(Player {
            status: Status::Playing,
            name: format!("Player {}", i + 1),
            roll_sum: 0,
            points: 0,
        });
        //println!("{}",format!("{}", player_count).as_str())
    }
    //Create game
    Game { turn: 0, players, winner: None }
}
