use crate::logic;
use crate::logic::{Game, Status};
use cursive::align::HAlign::Center;
use cursive::theme::BaseColor::{Black, Red, White};
use cursive::theme::BorderStyle::Simple;
use cursive::theme::Color::{Dark, Light};
use cursive::theme::PaletteColor::{Background, Highlight, HighlightText, Primary, View};
use cursive::theme::{Palette, Theme};
use cursive::traits::*;
use cursive::views::{
    Button, Dialog, DummyView, Layer, LinearLayout, ScrollView, SelectView, ThemedView,
};
use cursive::Cursive;

pub fn render() {
    //Define the window root
    let mut root = cursive::default();
    root.add_global_callback('q', |r| r.quit());

    //Set theme
    let mut palette = Palette::default();
    palette[Background] = Dark(Black);
    palette[View] = Light(White);
    palette[Highlight] = Dark(Red);
    palette[HighlightText] = Light(White);
    root.set_theme(Theme {
        shadow: false,
        borders: Simple,
        palette,
    });

    let greeting = Dialog::text(
        "Welcome to Blackchuck! \n\
        Please select the amount of players.",
    );

    //Make buttons choose how many players the game will have
    let mut select_players = SelectView::<String>::new().on_submit(|r, item: &str| {
        let game = logic::create_game(item.parse().expect("Could not parse selection"));
        Cursive::set_user_data(r, game);
        play_turn(r, false, false)
    });
    //Create 4 buttons
    for i in 1..4 {
        select_players.add_item((&i + 1).to_string(), (&i + 1).to_string())
    }

    let selection_container = LinearLayout::horizontal()
        .child(greeting)
        .child(DummyView)
        .child(select_players.h_align(Center).min_width(10))
        .child(DummyView);

    root.add_layer(
        Dialog::around(LinearLayout::vertical().child(selection_container))
            .padding_lrtb(2, 2, 1, 1)
            .button("Quit", Cursive::quit)
            .h_align(Center)
            .title("Select players"),
    );

    root.run()
}

fn play_turn(r: &mut Cursive, end_turn: bool, roll_dice: bool) {
    let game = r.user_data::<Game>().expect("Could not read game data");
    //End the turn if requested by the previous iteration. This can't be done inside the
    //buttons, because the variables can't be accessed inside the closures.
    if end_turn {
        game.end_turn();
    }

    //Clone the game, to read its properties later
    //This is needed because the values can't be read inside the match statement directly
    game.whose_turn();
    let cloned_game = &mut game.clone();

    match game.whose_turn() {
        Some(player) => {
            if roll_dice {
                player.roll_dice();
            }
            //The cloned game must be updated
            cloned_game.players[cloned_game.turn as usize] = player.clone();
            cloned_game.end_turn();

            if player.status == Status::Bust {
                //Show popup if the current player is bust
                let popup_text = Dialog::text(format!(
                    "{} is bust! \nRoll sum: {}",
                    player.name, player.roll_sum
                ));
                let popup_btn = Button::new("Ok", |r| play_turn(r, false, false));

                r.pop_layer();
                r.add_layer(Dialog::around(
                    LinearLayout::vertical().child(popup_text).child(popup_btn),
                ));
            } else if player.status == Status::TwentyOne {
                play_turn(r, false, false)
            } else {
                //Define widgets
                let reroll_btn = Button::new("Roll dice!", |r| play_turn(r, false, true));
                let end_turn_btn = Button::new("End Turn", |r| play_turn(r, true, false));
                let player_box =
                    Dialog::text(format!("{} \nRoll sum: {}", player.name, player.roll_sum))
                        .min_width(17);

                //Add player list
                let mut player_list = LinearLayout::vertical();
                for each in cloned_game.clone().players {
                    //Change list colors based on whose turn it is
                    let mut palette = Palette::default();
                    let borders = Simple;

                    let player_element = Dialog::text(format!(
                        "{} \nRoll sum: {} \n{}",
                        each.name, each.roll_sum, each.status
                    ))
                    .min_width(17);

                    if each == player.clone() {
                        palette[View] = Dark(Red);
                        palette[Primary] = Light(White);
                    } else {
                        palette[View] = Light(White)
                    }

                    //The Layer fills the blanks from the TextView, so it can be properly themed
                    let themed_element = ThemedView::new(
                        Theme {
                            shadow: false,
                            borders,
                            palette,
                        },
                        Layer::new(player_element),
                    );
                    //Add the element to the list
                    player_list.add_child(themed_element);
                }
                let scrollable_player_list = ScrollView::new(player_list);

                let game_controls = LinearLayout::vertical()
                    .child(player_box)
                    .child(reroll_btn)
                    .child(end_turn_btn)
                    .child(DummyView)
                    .child(Button::new("Quit", Cursive::quit));

                //Draw widgets
                let window = Dialog::around(
                    LinearLayout::horizontal()
                        .child(game_controls)
                        .child(DummyView)
                        .child(Dialog::around(scrollable_player_list)),
                );
                r.pop_layer();
                r.add_layer(window);
            }
        }
        None => {
            let popup_text = Dialog::text(format!(
                "{} won the round!",
                match game.clone().winner {
                    Some(winner) => winner.name,
                    None => String::from("Nobody"),
                }
            ));

            let popup_btn = Button::new("Play again", |r| play_turn(r, false, false));

            game.winner = None;
            game.reset();

            r.pop_layer();
            r.add_layer(
                Dialog::around(LinearLayout::vertical().child(popup_text).child(popup_btn))
                    .button("Quit", |r| r.quit()),
            );
        }
    }
}
